/*
 * UTEmuDataLocations.h
 *
 *  Created on: Oct 14, 2014 
 *  Uptaded on: March, 2020 
 *      Author: ADendek
 *      Author: WKrupa (wokrupa@cern.ch)
 */
#pragma once
#pragma GCC diagnostic ignored "-Wunused-variable"

#include <string>

namespace UTEmu
{
namespace DataLocations
{
static const unsigned int number_of_asics = 5;
static const std::string &UTEmu_path = UTEMU_PATH;
// should be extended
static const std::string &RawTES = "UTEmu/Raw";
static const std::string &HeaderTES = "UTEmu/Header";
static const std::string &PedestalTES = "UTEmu/Pedestal";
static const std::string &CMSTES = "UTEmu/CMS";
static const std::string &ZSTES = "UTEmu/ZS";
static const std::string &Clusters_TES = "UTEmu/Clusters";
static const std::string &KeplerCluster_TES = "UTEmu/KeplerClusters";
static const std::string &MaskLocation =
    UTEmu_path + "UTEmu/UTEmuOptions/options/UT/Masks.dat";
static const std::string &PedestalLocation= "UTEmu/PedestalLocation";
static const std::string &NoiseTreshold = UTEmu_path + "UTEmu/UTEmuOptions/options/UT/Noise.dat";
} // namespace DataLocations
} // namespace UTEmu
