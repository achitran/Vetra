/*
 *  UTEmuRawDataReaderAlgorithm.h
 *
 *      Created   : March, 2020
 *      Author: WKrupa (wokrupa@cern.ch)
 */

#pragma once

#include "GaudiAlg/GaudiAlgorithm.h"

// Local
#include "UTEmu/UTEmuDataType.h"
#include "UTEmuFileValidator.h"
#include "UTEmuRawDataFactory.h"

#include <string>

namespace UTEmu {

class RawDataReaderAlgorithm : public GaudiAlgorithm {
 public:
  RawDataReaderAlgorithm(const std::string& name, ISvcLocator* pSvcLocator);

  virtual StatusCode initialize() override;
  virtual StatusCode execute() override;

 private:
  void termiateApp();

  bool m_isStandalone;
  unsigned int m_eventNumber;
  unsigned int m_skipEventNumber;

  std::string m_inputDataOption;
  std::string m_InputData;
  std::string m_outputLocation;
 
  int m_sensorNumber;
  unsigned int m_numOfASICs;

  double m_mean;
  double m_sigma;

  std::vector<RawDataContainer<>> RawDataContaine_array;
  UTEmu::FileValidator m_fileValidator;
  UTEmu::RawDataFactory::DataReaderPtr m_rawDataReader;
  UTEmu::RawDataFactory m_inputDataFactory;
};
}
