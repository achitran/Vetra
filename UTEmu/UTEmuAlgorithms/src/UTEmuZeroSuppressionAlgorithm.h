/*
 * UTEmuZeroSuppressionAlgorithm.cpp
 *
 *  Created on: Oct 14, 2014 
 *  Uptaded on: March, 2020 
 *      Author: ADendek
 *      Author: WKrupa (wokrupa@cern.ch)
 */

#pragma once
#include "GaudiAlg/GaudiAlgorithm.h"
#include "UTEmu/UTEmuDataType.h"
#include "UTEmuChannelMaskProvider.h"
#include "UTEmuChannelMaskFileValidator.h"
#include "UTEmuTresholdProvider.h"
#include "UTEmuZeroSuppressor.h"
#include <string>
#include <memory.h>

namespace UTEmu
{

class ZeroSuppressionAlgorithm : public GaudiAlgorithm
{

public:
  ZeroSuppressionAlgorithm(const std::string &name,
                           ISvcLocator *pSvcLocator);

  virtual StatusCode initialize() override;
  virtual StatusCode execute() override;
  virtual StatusCode finalize() override;

private:
  StatusCode initializeBase();
  StatusCode retriveMasksFromFile();
  StatusCode getData(std::string);
  void suppressZeros(unsigned int);
  StatusCode retreiveNoise(unsigned int);
  bool m_isStandalone;
  RawDataMap<double> *m_dataMap;
  RawData<double> *m_data;
  RawZSDataMap *m_outputZSDataMap;
  std::string m_inputDataLocation;
  std::string m_outputDataLocation;
  std::vector<std::string> m_noiseOutputLocations;

  unsigned int num_of_asics;
  int m_event;
  int m_treningEventNumber;
  int m_skippEvent;
  unsigned int m_numofAsics;
  bool m_debug;
  double m_lowThreshold;
  double m_highThreshold;

  std::string m_channelMaskInputLocation;
  std::vector<ChannelMaskFileValidator> m_channelMaskFileValidator;
  std::vector<ChannelMaskProvider> m_channelMaskProvider;
  std::vector<ZeroSuppressor> m_zeroSuppressor;
  std::vector<TresholdProvider> m_zsThresholdProvider;
};
} // namespace UTEmu
