/*
 * UTEmuCommonModeSubtractorAlgorithm.cpp
 *
 *  Created on: Oct 14, 2014 
 *  Uptaded on: March, 2020 
 *      Author: ADendek
 *      Author: WKrupa (wokrupa@cern.ch)
 */

#include "UTEmuCommonModeSubtractorAlgorithm.h"

using namespace UTEmu;

DECLARE_COMPONENT(UTEmu::CommonModeSubtractorAlgorithm)

CommonModeSubtractorAlgorithm::CommonModeSubtractorAlgorithm(
    const std::string &name, ISvcLocator *pSvcLocator)
    : GaudiAlgorithm(name, pSvcLocator),
      m_data(),
      m_channelMaskInputLocation(),
      m_CMSOption(),
      m_noiseCalculatorType(),
      m_event(0),
      m_skipEvent(0),
      m_numofAsics(),
      m_channelMaskFileValidator(),
      m_channelMaskProvider()
{
  declareProperty("InputDataLocation",
                  m_inputDataLocation = UTEmu::DataLocations::PedestalTES);
  declareProperty("OutputDataLocation",
                  m_outputDataLocation = UTEmu::DataLocations::CMSTES);
  declareProperty("CMSType", m_CMSOption = UTEmu::CMSType::Iteratively);
  declareProperty("skippEventNumber", m_skipEvent = 0);
  declareProperty(
      "ChannelMaskInputLocation",
      m_channelMaskInputLocation = UTEmu::DataLocations::MaskLocation);
  declareProperty(
      "NoiseCalculatorType",
      m_noiseCalculatorType = UTEmu::NoiseCalculatorType::calculator);
  declareProperty("NumberofAsics", m_numofAsics = UTEmu::DataLocations::number_of_asics);
}

StatusCode CommonModeSubtractorAlgorithm::initialize()
{
  if (StatusCode::SUCCESS != initializeBase())
    return StatusCode::FAILURE;
  if (StatusCode::SUCCESS != buildSubtractorEngine())
    return StatusCode::FAILURE;
  if (StatusCode::SUCCESS != buildNoiseCalculator())
    return StatusCode::FAILURE;
  if (StatusCode::SUCCESS != retriveMasksFromFile())
    return StatusCode::FAILURE;

  info() << "Initialized successfully my friend!" << endmsg;
  return StatusCode::SUCCESS;
}

StatusCode CommonModeSubtractorAlgorithm::execute()
{
  m_outputDataMap = new RawDataMap<double>();

  if (StatusCode::SUCCESS != getData(m_inputDataLocation))
    return StatusCode::FAILURE;
  if (m_dataMap->isEmpty())
  {
    put(m_outputDataMap, m_outputDataLocation);
    return StatusCode::SUCCESS;
  }
  for (const auto &[asic_num, Data] : m_dataMap->getData())
  {
    m_data = new RawData<>(Data);
    processEvent(asic_num);

    delete m_data;
  }
  m_event++;
  put(m_outputDataMap, m_outputDataLocation);

  return StatusCode::SUCCESS;
}

StatusCode CommonModeSubtractorAlgorithm::finalize()
{
  if (StatusCode::SUCCESS != saveNoiseToFile())
    return StatusCode::FAILURE;
  return GaudiAlgorithm::finalize();
}

StatusCode CommonModeSubtractorAlgorithm::initializeBase()
{

  for (unsigned int asic_num = 0; asic_num < m_numofAsics; asic_num++)
  {
    auto channel_maskValidator = ChannelMaskFileValidator(m_channelMaskInputLocation);
    m_channelMaskFileValidator.push_back(channel_maskValidator);
  }

  for (unsigned int asic_num = 0; asic_num < m_numofAsics; asic_num++)
  {
    auto channel_maskProvider =  ChannelMaskProvider(m_channelMaskFileValidator[asic_num]);
    m_channelMaskProvider.push_back(channel_maskProvider);
  }

  for (unsigned int asic_num = 0; asic_num < m_numofAsics; asic_num++)
  {
    auto noise_location = UTEmu::DataLocations::UTEmu_path + "UTEmu/UTEmuOptions/options/UT/Noise_ASIC" + std::to_string(asic_num) + ".dat";
    m_noiseOutputLocations.push_back(noise_location);

    auto noise_beforeCMS_location = UTEmu::DataLocations::UTEmu_path + "UTEmu/UTEmuOptions/options/UT/Noise_beforeCMS_ASIC" + std::to_string(asic_num) + ".dat";
    m_noiseBeforeCMSOutputLocations.push_back(noise_beforeCMS_location);

    auto CMS_subtractor = CommonModeSubtractorFactory(m_channelMaskProvider[asic_num]);
    m_SubtractorFactories.push_back(CMS_subtractor);
  }

  for (unsigned int asic_num = 0; asic_num < m_numofAsics; asic_num++)
  {
    auto noise_calculator_factory = NoiseCalculatorFactory();
    m_noiseCalculatorFactories.push_back(noise_calculator_factory);
  }

  for (unsigned int asic_num = 0; asic_num < m_numofAsics; asic_num++)
  {
    auto noise_calculator_ptr = NoiseCalculatorFactory::NoiseCalcualtorPtr();
    m_noiseCalculatorPtrs.push_back(noise_calculator_ptr);

    auto noise_beforeCMS_calculator_ptr = NoiseCalculatorFactory::NoiseCalcualtorPtr();
    m_noise_beforeCMS_CalculatorPtrs.push_back(noise_beforeCMS_calculator_ptr);

    auto CMS_subtractor_ptr = CMSubtractorPtr();
    m_CMSEnginePtrs.push_back(CMS_subtractor_ptr);
  }

  return GaudiAlgorithm::initialize();
}

StatusCode CommonModeSubtractorAlgorithm::buildSubtractorEngine()
try
{
  for (unsigned int asic_num = 0; asic_num < m_numofAsics; asic_num++)
    m_CMSEnginePtrs[asic_num].reset(m_SubtractorFactories[asic_num].createCMSubtractor(m_CMSOption));
  return StatusCode::SUCCESS;
}
catch (CommonModeSubtractorFactory::NoSuchState &exception)
{
  error() << "Invalid CommonMode Engine Type  : " << exception.what() << endmsg;
  return StatusCode::FAILURE;
}

StatusCode CommonModeSubtractorAlgorithm::buildNoiseCalculator()
try
{
  for (unsigned int asic_num = 0; asic_num < m_numofAsics; asic_num++)
  {
    m_noiseCalculatorPtrs[asic_num] =
        m_noiseCalculatorFactories[asic_num].createNoiseCalculator(m_noiseCalculatorType);

    m_noise_beforeCMS_CalculatorPtrs[asic_num] =
        m_noise_beforeCMS_CalculatorFactories[asic_num].createNoiseCalculator(m_noiseCalculatorType);
  }

  return StatusCode::SUCCESS;
}
catch (NoiseCalculatorFactory::NoSuchState &exception)
{
  error() << "Invalid NoiseCalculator Type  : " << exception.what() << endmsg;
  return StatusCode::FAILURE;
}

StatusCode CommonModeSubtractorAlgorithm::retriveMasksFromFile()
try
{
  for (unsigned int asic_num = 0; asic_num < m_numofAsics; asic_num++)
    m_channelMaskProvider[asic_num].getMaskFromFile(m_channelMaskInputLocation);
  return StatusCode::SUCCESS;
}
catch (ChannelMaskProvider::InputFileError &exception)
{
  error() << "Channel Mask File Input Error: " << m_channelMaskInputLocation
          << endmsg;
  return StatusCode::FAILURE;
}

StatusCode CommonModeSubtractorAlgorithm::getData(std::string input_loc)
{
  m_dataMap = getIfExists<RawDataMap<>>(input_loc);
  if (!m_dataMap)
  {
    error() << " ==> There is no input data: " << input_loc << endmsg;
    return StatusCode::FAILURE;
  }
  return StatusCode::SUCCESS;
}

void CommonModeSubtractorAlgorithm::processEvent(int asic_num)
{
  RawData<double> *l_afterCMS = new RawData<double>();
  m_CMSEnginePtrs[asic_num]->processEvent(m_data, &l_afterCMS);
  m_noise_beforeCMS_CalculatorPtrs[asic_num]->updateNoise(m_data);
  m_noiseCalculatorPtrs[asic_num]->updateNoise(l_afterCMS);
  m_outputDataMap->addData(asic_num, *l_afterCMS);
}

StatusCode CommonModeSubtractorAlgorithm::skippTreaningEvent()
{
  return StatusCode::SUCCESS;
}

StatusCode CommonModeSubtractorAlgorithm::saveNoiseToFile()
try
{
  for (unsigned int asic_num = 0; asic_num < m_numofAsics; asic_num++)
  {
    m_noiseCalculatorPtrs[asic_num]->saveNoiseToFile(m_noiseOutputLocations[asic_num]);
    m_noise_beforeCMS_CalculatorPtrs[asic_num]->saveNoiseToFile(m_noiseBeforeCMSOutputLocations[asic_num]);
  }
  return StatusCode::SUCCESS;
}
catch (Noise::NoiseCalculatorError &ex)
{
  error() << ex.what() << endmsg;
  return StatusCode::FAILURE;
}
