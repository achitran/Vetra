/*
 * UTEmuZeroSuppressionAlgorithm.cpp
 *
 *  Created on: Oct 14, 2014 
 *  Uptaded on: March, 2020 
 *      Author: ADendek
 *      Author: WKrupa (wokrupa@cern.ch)
 */

#include "UTEmuZeroSuppressionAlgorithm.h"
#include "UTEmu/UTEmuDataLocations.h"

using namespace UTEmu;

DECLARE_COMPONENT(UTEmu::ZeroSuppressionAlgorithm)

ZeroSuppressionAlgorithm::ZeroSuppressionAlgorithm(
    const std::string &name, ISvcLocator *pSvcLocator)
    : GaudiAlgorithm(name, pSvcLocator),
      m_isStandalone(true),
      m_data(),
      m_event(0),
      m_skippEvent(10),
      m_debug(),
      m_lowThreshold(),
      m_highThreshold(),
      m_channelMaskInputLocation()

{
  declareProperty("SkippEventNumber", m_skippEvent = 0);
  declareProperty(
      "InputDataLocation",
      m_inputDataLocation = UTEmu::DataLocations::CMSTES);
  declareProperty(
      "OutputDataLocation",
      m_outputDataLocation = UTEmu::DataLocations::ZSTES);
  declareProperty(
      "ChannelMaskInputLocation",
      m_channelMaskInputLocation = UTEmu::DataLocations::MaskLocation);
  declareProperty("standalone", m_isStandalone = true);
  declareProperty("NumberofAsics", m_numofAsics = UTEmu::DataLocations::number_of_asics);
  declareProperty("Debug", m_debug = false);
  declareProperty("LowThreshold", m_lowThreshold = 3);
  declareProperty("HighThreshold", m_highThreshold = 4);
}

StatusCode ZeroSuppressionAlgorithm::initialize()
{
  if (StatusCode::SUCCESS != initializeBase())
    return StatusCode::FAILURE;

  if (StatusCode::SUCCESS != retriveMasksFromFile())
    return StatusCode::FAILURE;

  info() << "Initialized successfully!" << endmsg;
  return StatusCode::SUCCESS;
}

StatusCode ZeroSuppressionAlgorithm::execute()
{

  m_outputZSDataMap = new RawZSDataMap();

  if (StatusCode::SUCCESS != getData(m_inputDataLocation))
    return StatusCode::FAILURE;
  if (m_dataMap->isEmpty())
  {
    info() << "zs put empty" << endmsg;
    put(m_outputZSDataMap, m_outputDataLocation);
    return StatusCode::SUCCESS;
  }

  for (const auto &[asic_num, Data] : m_dataMap->getData())
  {
    m_data = new RawData<double>(Data);
    suppressZeros(asic_num);
    delete m_data;
  }

  m_event++;
  put(m_outputZSDataMap, m_outputDataLocation);
  return StatusCode::SUCCESS;
}

StatusCode ZeroSuppressionAlgorithm::finalize()
{
  return GaudiAlgorithm::finalize();
}

StatusCode ZeroSuppressionAlgorithm::initializeBase()
{
  for (unsigned int asic_num = 0; asic_num < m_numofAsics; asic_num++)
  {
    auto noise_location = UTEmu::DataLocations::UTEmu_path + "UTEmu/UTEmuOptions/options/UT/Noise_ASIC" + std::to_string(asic_num) + ".dat";
    m_noiseOutputLocations.push_back(noise_location);
  }

  for (unsigned int asic_num = 0; asic_num < m_numofAsics; asic_num++)
  {
    auto mask_validator = ChannelMaskFileValidator(m_channelMaskInputLocation);
    m_channelMaskFileValidator.push_back(mask_validator);

    auto threshold_provider = TresholdProvider(m_noiseOutputLocations[asic_num], m_lowThreshold, m_highThreshold);
    m_zsThresholdProvider.push_back(threshold_provider);
  }

  for (unsigned int asic_num = 0; asic_num < m_numofAsics; asic_num++)
  {
    auto mask_provider = ChannelMaskProvider(m_channelMaskFileValidator[asic_num]);
    m_channelMaskProvider.push_back(mask_provider);
  }

  for (unsigned int asic_num = 0; asic_num < m_numofAsics; asic_num++)
  {
    auto zero_suppresor = ZeroSuppressor(m_channelMaskProvider[asic_num], m_zsThresholdProvider[asic_num]);
    m_zeroSuppressor.push_back(zero_suppresor);
  }

  return GaudiAlgorithm::initialize();
}

StatusCode ZeroSuppressionAlgorithm::retriveMasksFromFile()
try
{
  for (unsigned int asic_num = 0; asic_num < m_numofAsics; asic_num++)
    (m_channelMaskProvider[asic_num]).getMaskFromFile(m_channelMaskInputLocation);
  return StatusCode::SUCCESS;
}
catch (ChannelMaskProvider::InputFileError &exception)
{
  error() << "Channel Mask File Input Error: " << m_channelMaskInputLocation
          << endmsg;
  return StatusCode::FAILURE;
}

StatusCode ZeroSuppressionAlgorithm::getData(std::string input_loc)
{
  m_dataMap = getIfExists<RawDataMap<double>>(input_loc);
  if (!m_dataMap)
  {
    error() << " ==> There is no input data: " << input_loc << endmsg;
    return StatusCode::FAILURE;
  }
  return StatusCode::SUCCESS;
}

void ZeroSuppressionAlgorithm::suppressZeros(unsigned int asic_num)
{
  RawZSData *aftersupression = new RawZSData();
  m_zeroSuppressor[asic_num].processEvent(m_data, &aftersupression);
  m_outputZSDataMap->addData(asic_num, *aftersupression);
}