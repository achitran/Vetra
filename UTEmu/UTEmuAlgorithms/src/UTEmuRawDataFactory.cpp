/*
 * UTEmuRawDataFactory.cpp
 *
 *  Created on: Mar 16, 2015
 *      Author: ADendek
 */

#include "UTEmuRawDataFactory.h"
#include "UTEmuRandomNoiseGenerator.h"
#include "UTEmuTxtDataReader.h"
#include "UTEmuDataReader.h"

using namespace UTEmu;

RawDataFactory::RawDataFactory(std::string& p_filename,
                               IFileValidator& p_fileValidator, unsigned int& p_numOfASICs,
                               double& p_mean, double& p_sigma)
    : m_filename(p_filename),
      m_fileValidator(p_fileValidator),
      m_numOfASICs(p_numOfASICs),
      m_mean(p_mean),
      m_sigma(p_sigma) {}

RawDataFactory::DataReaderPtr RawDataFactory::createDataEngine(
    const std::string& p_inputDataOption) {
  if (p_inputDataOption == InputDataOption::NoiseGenerator)
    return DataReaderPtr(new RandomNoiseGenerator(m_mean, m_sigma));
  if (p_inputDataOption == InputDataOption::TxT)
    return DataReaderPtr(new TxtDataReader(m_filename, m_fileValidator)); 
   if (p_inputDataOption == InputDataOption::Binary)
    return DataReaderPtr(new DataReader(m_filename, m_fileValidator, m_numOfASICs));       
  else
    throw NoSuchState(p_inputDataOption);
}
