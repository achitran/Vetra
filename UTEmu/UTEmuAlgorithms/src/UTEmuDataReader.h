/*
 * UTEmuMamdaDataReader.h
 *
 *  Created on: March, 2020 
 *      Author: WKrupa (wokrupa@cern.ch)
 */

#pragma once

#include "UTEmuIDataReader.h"
#include "UTEmuIFileValidator.h"
#include "UTEmu/UTEmuDataType.h"
#include <string>
#include <fstream>
#include <vector>
#include <map>
#include <stdio.h>
#include <memory>
#include <stdio.h>      
#include <stdlib.h>     
#include <time.h>  
#include <bitset>
#include <algorithm>
#include <stdlib.h>    
#include <time.h>     

struct data_asics{

    bool NZS = 0;
    bool IDLE = 0;
    signed int adc[4][4];
};

namespace UTEmu {

class DataReader : public IDataReader {
 public:
  DataReader(std::string& p_fileName, IFileValidator& p_fileValidator, unsigned int & p_numOfASICs);
  std::array<RawData<>, 4> * getEventData() override;
  void checkInput() override;
  data_asics decodeData();

  bool IDLE;
  bool NZS;

 private:

  IFileValidator& m_fileValidator;
  std::string& m_fileName;
  unsigned int & m_numOfASICs;
  FILE* inputFile;
  std::ofstream myfile;

};

} /* namespace UTEmu */
