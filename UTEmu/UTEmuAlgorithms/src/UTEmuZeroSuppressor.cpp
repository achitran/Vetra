/*
 * UTEmuZeroSuppressor.cpp
 *
 *  Created on: Oct 14, 2014
 *      Author: ADendek
 */

#include "UTEmuZeroSuppressor.h"
#include <iostream>

using namespace UTEmu;

ZeroSuppressor::ZeroSuppressor(IChannelMaskProvider &p_masksProvider, TresholdProvider &p_tresholdProvider)
    : m_masksProvider(p_masksProvider),
      m_tresholdProvider(p_tresholdProvider) {}

void ZeroSuppressor::processEvent(RawData<double> *p_data, RawZSData **p_output)
{
  retreiveNoise();
  int l_channelNumber = RawData<>::getnChannelNumber();
  RawData<>::DataType l_maskedChannelValue = 0;

  for (int channel = 0; channel < l_channelNumber; channel++)
  {
    if (!m_masksProvider.isMasked(channel))
    {
      if (p_data->getSignal(channel) > m_tresholdProvider.getHighClusterThreshold(channel))
        (*p_output)->setSignal(channel, p_data->getSignal(channel));
    }
  }
}

void ZeroSuppressor::retreiveNoise() try
{
  m_tresholdProvider.retreiveTresholds();
}
catch (ITresholdProvider::ThresholdProviderError &err)
{
  std::cout << "Cannot open Noise file: " << err.what() << std::endl;
}