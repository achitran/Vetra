/*
 * UTEmuPedestalSubtractorAlgorithm.cpp
 *
 *  Created on: Oct 14, 2014 
 *  Uptaded on: March, 2020 
 *      Author: ADendek
 *      Author: WKrupa (wokrupa@cern.ch)
 */

#pragma once
#include "GaudiAlg/GaudiAlgorithm.h"
#include "UTEmu/UTEmuDataType.h"
#include "UTEmuPedestalFollowingFactory.h"
#include "UTEmuIPedestalFollowing.h"
#include "UTEmuPedestalSubtractor.h"
#include "UTEmuChannelMaskProvider.h"
#include "UTEmuChannelMaskFileValidator.h"
#include "UTEmuPedestal.h"
#include "UTEmuPedestalFileValidator.h"
#include <string>
#include <memory.h>

namespace UTEmu
{

class PedestalSubtractorAlgorithm : public GaudiAlgorithm
{
  typedef std::shared_ptr<IPedestalFollowing> PedestalFollowingPtr;

  enum RunPhase
  {
    SKIPP,
    TREANING,
    SUBTRACTION
  };

public:
  PedestalSubtractorAlgorithm(const std::string &name,
                              ISvcLocator *pSvcLocator);

  virtual StatusCode initialize() override;
  virtual StatusCode execute() override;
  virtual StatusCode finalize() override;

private:
  StatusCode initializeBase();
  StatusCode buildFollowing();
  StatusCode retriveMasksFromFile();

  void skippEvent();
  void processTreaning(unsigned int);
  void subtractPedestals(unsigned int);

  StatusCode getData(std::string);
  void processAndSaveDataToTES(unsigned int);
  StatusCode savePedestalsToFile();

  RunPhase getRunPhase();

  bool m_isStandalone;

  RawDataMap<> *m_dataMap;
  RawData<> *m_data;
  RawDataMap<> *m_outputDataMap;

  std::string m_inputDataLocation; //to be removed later
  unsigned int num_of_asics;
  std::vector<std::string> PedestalLocations;
  std::string m_PedestalTES;
  std::string m_channelMaskInputLocation;
  std::string m_followingOption;
  int m_event;
  int m_treningEventNumber;
  int m_skippEvent;
  unsigned int  m_numofAsics;
  bool m_debug;

  std::vector<ChannelMaskFileValidator> m_channelMaskFileValidator;
  std::vector<ChannelMaskProvider> m_channelMaskProvider;
  std::vector<Pedestal> m_pedestals;
  std::vector<PedestalFollowingPtr> m_pedestalFollowingPtrs;
  std::vector<PedestalFileValidator> m_pedestalFileValidators;
  std::vector<PedestalFollowingFactory> m_followingFactories;
  std::vector<PedestalSubtractor> m_pedestalSubtractors;

};
} // namespace UTEmu
