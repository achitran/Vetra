/*
 * UTEmuFileValidator.h
 *
 *  Created on: Oct 5, 2014
 *      Author: ADendek
 */

#pragma once
#include "UTEmuIFileValidator.h"
#include <filesystem>
#include <string>

namespace UTEmu {
class FileValidator : public IFileValidator {
 public:
  FileValidator(std::string& p_filename);
  bool validateFile() override;

 private:
  bool isfileExist();
  bool isRegularFile();
  bool hasNonZeroSize();

  std::string& m_filename;
  std::filesystem::path m_path;
};
}
