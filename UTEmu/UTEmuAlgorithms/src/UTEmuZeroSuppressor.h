/*
 * UTEmuZeroSuppressor.h
 *
 *  Created on: Oct 14, 2014
 *      Author: ADendek
 */

#pragma once

#include "UTEmuIChannelMaskProvider.h"
#include "UTEmu/UTEmuDataType.h"
#include "UTEmuTresholdProvider.h"

namespace UTEmu {

class ZeroSuppressor{
 public:
  ZeroSuppressor(IChannelMaskProvider& p_masksProvider, TresholdProvider &p_tresholdProvider);
  void processEvent(RawData<double>* p_data, RawZSData** p_output);
  void retreiveNoise();
  
 private:
  IChannelMaskProvider& m_masksProvider;
  TresholdProvider &m_tresholdProvider;
};
}
