/*
 * UTEmuChannelMaskFileValidator.h
 *
 *  Created on: Oct 10, 2014
 *      Author: ADendek
 */

#pragma once
#include "UTEmuIFileValidator.h"
#include <filesystem>
#include <string>

namespace UTEmu {
class ChannelMaskFileValidator : public IFileValidator {
 public:
  ChannelMaskFileValidator(std::string& p_filename);
  bool validateFile() override;

 private:
  bool isfileExist();
  bool isRegularFile();
  bool hasProperSize();

  std::string& m_filename;
  std::filesystem::path m_path;
};
}
