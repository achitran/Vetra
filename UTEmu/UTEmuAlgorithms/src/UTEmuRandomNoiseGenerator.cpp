/*
 * UTEmuRandomNoiseGenerator.cpp
 *
 *  Created on: Mar 16, 2015
 *      Author: ADendek
 */

#include "UTEmuRandomNoiseGenerator.h"
#include <iostream>

using namespace UTEmu;

RandomNoiseGenerator::RandomNoiseGenerator(double& p_sigma, double& p_mean)
    : m_sigma(p_sigma), m_mean(p_mean), m_randomGenerator() {}

void RandomNoiseGenerator::checkInput() {}

std::array<RawData<>, 4>* RandomNoiseGenerator::getEventData() {
  std::array<RawData<>, 4>* l_outputNoise = new std::array<RawData<>, 4>();
  //for (int channel = 0; channel < RawData<>::getnChannelNumber(); channel++) {
  //  double l_randomNoise = m_randomGenerator.Gaus(m_sigma, m_mean);
  //  l_outputNoise->setSignal(l_randomNoise);
  //}
  return l_outputNoise;
}
