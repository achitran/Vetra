/*
 * UTEmuMamdaDataReader.cpp
 *
 *  Created on: March, 2020 
 *      Author: WKrupa (wokrupa@cern.ch)
 */


#include "UTEmuDataReader.h"
#include <iostream>
#include <iostream>
#include <string>
#include <vector>
#include <limits.h>
#include <cmath>
#include <bitset>

using namespace UTEmu;
using namespace std;

const uint32_t WORD1 = 1;
const uint32_t WORD2 = 2;
const uint32_t WORD3 = 3;
const uint32_t WORD6 = 6;
const uint32_t WORD4 = 4;
const uint32_t WORD8 = 8;
const uint32_t WORD12 = 12;
const uint32_t WORD16 = 16;
const uint32_t WORD32 = 32;
const uint32_t WORD24 = 24;
const uint32_t WORD20 = 20;
const uint32_t BxID1Mask = 0x00000fff;
const uint32_t BxID2Mask = 0x00000457;
const uint32_t IDMask = 0x000000ff;
const uint32_t adcMask = 0x0000003f;

const int num_of_asics = 4;
const int frame_len_in_bytes = 32;
const int frame_data_len_in_bytes = 12;
const int frame_data_1_len_in_bytes = 3;

template <std::size_t WORDN>
std::bitset<WORDN> reverse(std::bitset<WORDN> inbits)
{

  auto strbits = inbits.to_string();
  std::reverse(strbits.begin(), strbits.end());
  auto outbits = std::bitset<WORDN>(strbits);
  return (outbits);
}

DataReader::DataReader(std::string &p_fileName,
                       IFileValidator &p_fileValidator, unsigned int &p_numOfASICs)
    : m_fileValidator(p_fileValidator),
      m_fileName(p_fileName),
      m_numOfASICs(p_numOfASICs)
{
}

void DataReader::checkInput()
{
  if (!m_fileValidator.validateFile())
  {
    std::string errorMsg = "file validation error";
    throw IDataReader::InputFileError(errorMsg);
  }
  inputFile = fopen(m_fileName.c_str(), "rb");

  if (inputFile == NULL)
  {
    std::string errorMsg = "cannot open input file";
    throw IDataReader::InputFileError(errorMsg);
  }
}

std::array<RawData<>, 4> *DataReader::getEventData()
{

  if (feof(inputFile))
  {
    string l_errorMsg = "No more event!";
    throw IDataReader::NoMoreEvents(l_errorMsg);
  }

  std::array<RawData<>, 4> *outputData = new std::array<RawData<>, 4>();
  data_asics Data;

  while (!Data.IDLE)
    Data = decodeData();

  while (Data.IDLE)
  {
    Data = decodeData();
    IDLE = 1;
  }

  Data = decodeData();
  
  srand((unsigned)time(0)); 

  if (IDLE && !Data.IDLE)
  {
    std::array<RawData<>, 4> *DataArray = new std::array<RawData<>, 4>;

    for (int i = 0; i < 128; i++)
    {
      for (int j = 0; j < 4; j++)
      {
        (*DataArray)[j].setHeader0(2);
        (*DataArray)[j].setHeader1(1);
        (*DataArray)[j].setHeader2(1);
        (*DataArray)[j].setHeader3(1);
        (*DataArray)[j].setTime(1);
        (*DataArray)[j].setTDC(1);
        (*DataArray)[j].setNum(j);
      }
    }

    for (int j = 0; j < 4; j++)
    {
      (*DataArray)[j].setSignal(Data.adc[2][j]);
      (*DataArray)[j].setSignal(Data.adc[3][j]);
    }
    for (int i = 0; i < 31; i++)
    {
      Data = decodeData();
      for (int j = 0; j < 4; j++)
      {
        for (int i = 0; i < 4; i++)
          (*DataArray)[j].setSignal(Data.adc[i][j]);
      }
    }

    Data = decodeData();
    for (int j = 0; j < 4; j++)
    {
      (*DataArray)[j].setSignal(Data.adc[0][j]);
      (*DataArray)[j].setSignal(Data.adc[1][j]);

    }

    *outputData = *DataArray;
  }

  return outputData;
}

data_asics DataReader::decodeData()
{

  data_asics output_frame;

  unsigned char bytes_array[frame_len_in_bytes];
  unsigned char bytes_array_data_asics[num_of_asics][frame_data_1_len_in_bytes];

  fread(bytes_array, 1, frame_len_in_bytes, inputFile);

  for (int j = 0; j < num_of_asics; j++)
  {
    for (int i = (frame_len_in_bytes - frame_data_len_in_bytes + 3 * j); i < (frame_len_in_bytes - frame_data_len_in_bytes + 3 * (j + 1)); i++)
      bytes_array_data_asics[j][i - (frame_len_in_bytes - frame_data_len_in_bytes + 3 * j)] = bytes_array[i];
  }

  int frame_start_pos = 0;

  for (int j = 0; j < num_of_asics; j++)
  {
    for (int packet_rec_byte = 0; packet_rec_byte < 3; packet_rec_byte++)
    {
      std::bitset<WORD8> in_byte_1(bytes_array_data_asics[j][packet_rec_byte]);
      if (0)
        std::cout << (in_byte_1) << " ";
    }
  }

  uint32_t data_buff[num_of_asics];
  std::bitset<WORD24> data_body_field[num_of_asics];
  int data_body_int[num_of_asics];
  std::bitset<WORD12> bxid1_field[num_of_asics];
  std::bitset<WORD4> bxid2_field[num_of_asics];
  std::bitset<WORD8> id_field[num_of_asics];
  int id_int[num_of_asics];

  for (int j = 0; j < num_of_asics; j++)
  {

    data_buff[j] = (uint32_t)(((uint32_t)(bytes_array_data_asics[j][0]) & 0xff) << 16 |
                              ((uint32_t)(bytes_array_data_asics[j][1]) & 0xff) << 8 |
                              ((uint32_t)(bytes_array_data_asics[j][2]) & 0xff));

    data_body_field[j] = std::bitset<WORD24>(data_buff[j]);
    data_body_int[j] = (int)(data_body_field[j].to_ulong());
    bxid1_field[j] = std::bitset<WORD12>((data_body_int[j] >> 12) & BxID1Mask);
    bxid2_field[j] = std::bitset<WORD4>((data_body_int[j] >> 8) & BxID2Mask);
    id_field[j] = std::bitset<WORD8>((data_body_int[j]) & IDMask);
    id_int[j] = (int)(id_field[j].to_ulong());
  }

  if (id_int[0] == 240)
    output_frame.IDLE = 1;
  else
    output_frame.IDLE = 0;
  if (id_int[0] == 198 || id_int[0] == 70)
    output_frame.NZS = 1;
  else
    output_frame.NZS = 0;

  std::bitset<WORD6> adc_field[4][4];

  for (int j = 0; j < num_of_asics; j++)
  {

    adc_field[0][j] = std::bitset<WORD6>((data_body_int[j] >> 18) & adcMask);
    adc_field[1][j] = std::bitset<WORD6>((data_body_int[j] >> 12) & adcMask);
    adc_field[2][j] = std::bitset<WORD6>((data_body_int[j] >> 6) & adcMask);
    adc_field[3][j] = std::bitset<WORD6>((data_body_int[j]) & adcMask);

    for (int i = 0; i < 4; i++)
    {
      output_frame.adc[i][j] = adc_field[i][j].to_ulong();
      if (adc_field[i][j][5] == 1)
      {
        adc_field[i][j] = ~adc_field[i][j];
        output_frame.adc[i][j] = -1 * adc_field[i][j].to_ulong();
      }
    }
  }
  return output_frame;
}
