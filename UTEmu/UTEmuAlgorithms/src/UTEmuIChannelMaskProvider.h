/*
 * UTEmuIIChannelMaskProvider.h
 *
 *  Created on: Oct 10, 2014
 *      Author: ADendek
 */

#pragma once
#include <vector>
#include <string>
#include "UTEmu/UTEmuDataLocations.h"

namespace UTEmu {
class IChannelMaskProvider {
 public:
  virtual ~IChannelMaskProvider(){};
  virtual void getMaskFromFile(
      const std::string& p_filename = UTEmu::DataLocations::MaskLocation) = 0;
  virtual bool isMasked(int p_channel) = 0;
};
}
