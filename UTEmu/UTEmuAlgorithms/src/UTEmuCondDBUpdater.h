

#pragma once
#include "GaudiAlg/GaudiAlgorithm.h"
#include "UTEmu/UTEmuDataType.h"
#include "UTEmuHistoFileValidator.h"
#include <string>
#include <TH1F.h>
#include <TFile.h>

namespace UTEmu
{

class CondDBUpdater : public GaudiAlgorithm
{
public:
  CondDBUpdater(const std::string &name,
                           ISvcLocator *pSvcLocator);

  virtual StatusCode initialize() override;
  virtual StatusCode execute() override;
  virtual StatusCode finalize() override;

private:

  TFile *m_file;
  TH1F  *m_histo;
  std::string m_xmlFileName;
  std::string m_inputFileName;
  HistoFileValidator m_HistoFileValidator;
  unsigned int  m_numofAsics;

};
} // namespace UTEmu
