__author__ = 'W. Krupa'

from Gaudi.Configuration import *
from Configurables import UTEmuOptions

UTEmuOptions().HistoInputFile = '/home/physics/physusers/krupaw/Vetra/UTEmu-histos.root'
UTEmuOptions().CondDBFile = '/home/physics/physusers/krupaw/Vetra/UTEmu/UTEmuOptions/options/CondDB/UT.xml'
UTEmuOptions().CondUpdate = True
UTEmuOptions().EvtMax= 1000

