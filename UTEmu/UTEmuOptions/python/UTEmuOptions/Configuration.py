
"""
High level configuration tools for UTEmu standalone
This  file is based on Kepler configuration, see https://gitlab.cern.ch/lhcb/Kepler/blob/master/Kepler/python/Kepler/Configuration.py
"""
__version__ = "1.0"
__author__  = "A. Dendek"

from Gaudi.Configuration import *
import GaudiKernel.ProcessJobOptions
from Configurables import LHCbConfigurableUser, LHCbApp


from Configurables import UTEmu__RawDataReaderAlgorithm as rawDataReader
from Configurables import UTEmu__RawDataMonitorAlgorithm as rawDataMonitor
from Configurables import UTEmu__PedestalSubtractorAlgorithm  as pedestalSubtractor
from Configurables import UTEmu__PedestalSubtractorDataMonitorAlgorithm as pedestalMonitor
from Configurables import UTEmu__CommonModeSubtractorAlgorithm  as CMS
from Configurables import UTEmu__CommonModeSubtractorDataMonitorAlgorithm  as CMSMonitor
from Configurables import UTEmu__ClusterCreatorAlgorithm as ClusterCreator
from Configurables import UTEmu__ClusterCreatorDataMonitorAlgorithm as ClusterCreatorMonitor
from Configurables import UTEmu__ZeroSuppressionAlgorithm as ZeroSuppression
from Configurables import UTEmu__ZeroSuppressionDataMonitorAlgorithm as ZeroSuppressionMonitor
from Configurables import UTEmu__NTupleCreator as nTupleCreator
from Configurables import UTEmu__CondDBUpdater as CondDBUpdater


class UTEmuOptions(LHCbConfigurableUser):

    __slots__ = {
        # Input
        "InputFile"          :   " ",            # Names of input file 
        "CondDBFile"          :  " ",            # Names of condDB xml file
        "HistoInputFile"          :  " ",        # Name of histo input file for CondDB config
        "EvtMax"             :   -1,            # Max. number of events to process
        "isPedestal"         :   False,
        "isZS"               :   False,  
        "CondUpdate"         :   False,               
        # Output
        "HistogramFile"      :   "",            # Name of output histogram file
        "PedestalOutputData" :   "",            #
        "PedestalInputData" :   "",             #
        "TupleFile"          :   "",            # Name of output tuple file
        "WriteTuples"        :   False,         # Flag to write out tuple file or not
        # Options
        "Monitoring"         :   False,         # Flag to run the monitoring sequence
        "UserAlgorithms"     :   [],            # List of user algorithms not typically run
	    "EventsToDisplay"    :  100,             # number of events to save into output file 
        "Debug"    :  True                   # Mode

    }

    _propertyDocDct = { 
        'InputFile'       : """ Name of input raw data file """,
        'CondDBFile'       : """  Names of condDB xml file """,  
        'HistoInputFile'       : """  NName of histo input file for CondDB config """,  
        'isPedestal'      : """ Flag to run the pedestal calculation sequence """,
        'isZS'            : """ Flag to run the with ZS sequence """,
        'CondUpdate'      : """ Flag to run conddb update """,
        'EvtMax'          : """ Maximum number of events to process """,
        'HistogramFile'   : """ Name of output histogram file """,
        'PedestalOutputData': """ Name of output pedestal file """,
     	'PedestalInputData': """ location of the pre calcualted pedestal sums """,
        'TupleFile'       : """ Name of output tuple file """,
        'WriteTuples'     : """ Flag to write out tuple files or not """,
        'Monitoring'      : """ Flag to run the monitoring sequence or not """,
        'UserAlgorithms'  : """ List of user algorithms """,
        'Debug'           : """ Debugging mode """
       
    }

    __used_configurables__ = [LHCbApp,LHCbConfigurableUser]


    def configureSequence(self):
        """
        Set up the top level sequence and its phases
        """
        log.info("Configure sequence: start")
        UTEmu_seq = GaudiSequencer("UTEmuOptionsSequencer")
        ApplicationMgr().TopAlg = [UTEmu_seq]

        if self.getProp("CondUpdate") == True:
            log.info("Configure sequence: CondUpdate")
            cond_seq = GaudiSequencer("CondDBUpdate")
            self.configureCondDB(cond_seq)
            UTEmu_seq.Members += [cond_seq]   
        elif self.getProp("isPedestal") == True:
            log.info("Configure sequence: isPedestal")
            pedestal_seq = GaudiSequencer("Pedestal")
            self.configurePedestal(pedestal_seq)
            UTEmu_seq.Members += [pedestal_seq]
        else:
            log.info("Configure sequence: isRun")
            run_seq = GaudiSequencer("Run")
            self.configureRun(run_seq)
            UTEmu_seq.Members += [run_seq]
            if self.getProp("Monitoring") == True:
                log.info("Configure sequence: monitoring enabled")
                monitoringSeq = GaudiSequencer("Monitoring")
                self.configureMonitoring(monitoringSeq)
                UTEmu_seq.Members += [monitoringSeq]

        UserAlgorithms = self.getProp("UserAlgorithms")
        if (len(UserAlgorithms) != 0):
            userSeq = GaudiSequencer("UserSequence")
            userSeq.Members = UserAlgorithms
            UTEmu_seq.Members += [userSeq]
        log.info("Configure sequence: end")


    def configureInput(self):
        log.info("Configure input: start")
        # No events are read as input
        ApplicationMgr().EvtSel = 'NONE'
        # Delegate handling of max. number of events to ApplicationMgr
        self.setOtherProps(ApplicationMgr(), ["EvtMax"])
        # Transient store setup
        EventDataSvc().ForceLeaves = True
        EventDataSvc().RootCLID    =    1
        log.info("Configure input: end")


    def configureOutput(self):
        log.info("Configure output:  start")

        # ROOT persistency for histograms
        ApplicationMgr().HistogramPersistency = "ROOT"
        # Set histogram file name.
        histoFile = "UTEmu-histos.root"
        if (self.isPropertySet("HistogramFile") and self.getProp("HistogramFile") != ""):
            histoFile = self.getProp("HistogramFile")
        HistogramPersistencySvc().OutputFile = histoFile
        # Set tuple file name.
        tupleFile = "UTEmu-tuple.root"
        if (self.isPropertySet('TupleFile') and self.getProp("TupleFile") != ""):
            tupleFile = self.getProp("TupleFile")
        ApplicationMgr().ExtSvc += [NTupleSvc()]
        tupleStr = "FILE1 DATAFILE='%s' TYP='ROOT' OPT='NEW'" % tupleFile
        NTupleSvc().Output += [tupleStr]
        from Configurables import MessageSvc
        MessageSvc().setWarning += ['RFileCnv', 'RCWNTupleCnv']
        log.info("Configure output:  start")


    def configurePedestal(self, seq):
        rawDataReader().inputData= self.getProp("InputFile")
        pedestalSubtractor().ChannelMaskInputLocation= "$UTEMUOPTIONSROOT/options/UT/Masks.dat"
        pedestalSubtractor().treningEntry=400
        pedestalSubtractor().Debug=self.getProp("Debug")
        seq.Members =[rawDataReader(),pedestalSubtractor()]


    def configureCondDB(self, seq):
        #CondDBUpdater().inputData=self.getProp("InputFile")
        #pedestalSubtractor().ChannelMaskInputLocation= "$UTEMUOPTIONSROOT/options/UT/Masks.dat"
        #pedestalSubtractor().FollowingOption='file'
        #pedestalSubtractor().treningEntry=1
        #pedestalSubtractor().Debug=self.getProp("Debug")
        CondDBUpdater().CondDBFileName=self.getProp("CondDBFile")
        CondDBUpdater().HistoInputFileName=self.getProp("HistoInputFile")        
        seq.Members = [CondDBUpdater()]

    def configureRun(self, seq):
        rawDataReader().inputData=self.getProp("InputFile")
        pedestalSubtractor().ChannelMaskInputLocation= "$UTEMUOPTIONSROOT/options/UT/Masks.dat"
        pedestalSubtractor().FollowingOption='file'
        pedestalSubtractor().treningEntry=1
        pedestalSubtractor().Debug=self.getProp("Debug")
        CMS().ChannelMaskInputLocation = "$UTEMUOPTIONSROOT/options/UT/Masks.dat"
        nTupleCreator().StoreEventNumber=900000
        nTupleCreator().WriteRaw = False
        nTupleCreator().WriteHeader = True
        nTupleCreator().WritePedestal = False
        nTupleCreator().WriteCMS = False
        ZeroSuppression().ChannelMaskInputLocation= "$UTEMUOPTIONSROOT/options/UT/Masks.dat" #do not forget to remove it 
        seq.Members = [rawDataReader() ,pedestalSubtractor(), CMS()]
        if(self.getProp("isZS") == True):
            seq.Members += [ZeroSuppression()]


    def configureMonitoring(self, seq):
        rawDataMonitor().displayEventNumber=self.getProp("EventsToDisplay")
        rawDataMonitor().Debug=self.getProp("Debug")               
        pedestalMonitor().displayEventNumber=self.getProp("EventsToDisplay")
        CMSMonitor().displayEventNumber=self.getProp("EventsToDisplay")
        CMSMonitor().Debug=self.getProp("Debug")               
        seq.Members = [rawDataMonitor() ,pedestalMonitor(), CMSMonitor()]
        if(self.getProp("isZS") == True):
            seq.Members += [ZeroSuppressionMonitor()]

    def evtMax(self):
        return LHCbApp().evtMax()

    def __apply_configuration__(self):
        GaudiKernel.ProcessJobOptions.PrintOn()
        self.configureInput()
        self.configureSequence()
        if self.getProp("CondUpdate") == False:
            self.configureOutput()
        GaudiKernel.ProcessJobOptions.PrintOn()
        log.info(self)
        GaudiKernel.ProcessJobOptions.PrintOff()


