/*
 * UTEmuCommonModeSubtractorDataMonitorAlgorithm.h
 *
 *  Created on: Nov 26, 2014
 *      Author: ADendek
 */

#pragma once

#include "UTEmuDataMonitorAlgorithm.h"
#include "UTEmu/UTEmuNoise.h"
#include <map>

namespace UTEmu {

class CommonModeSubtractorDataMonitorAlgorithm : public DataMonitorAlgorithm {
 public:
  CommonModeSubtractorDataMonitorAlgorithm(const std::string& name,
                                           ISvcLocator* pSvcLocator);
  StatusCode execute() override;
  StatusCode finalize() override;

 private:
  typedef std::map<int, TH1D*> HistogramMap;
  typedef std::map<int, TH2D*> HistogramMap2D;

  StatusCode getData(std::string) override;

  StatusCode saveSimpleEvents() override;
  virtual StatusCode fillOnly2DHistogram() override;

  void createHistogram2D() override;
  TH2D* bookHistogram2D(const std::string& p_histogramName,
                        const std::string& p_histogramTitle,
                        int p_sensorNumber) override;

  std::string createHistogramTitle(unsigned int) override;
  std::string createHistogramName(unsigned int) override;

  void fillHistogram2D(unsigned int) override;
  void fillHistogram(TH1D* p_histogram) override;

  void createNoiseHistograms(unsigned int);
  void fillNoiseHistograms(unsigned int);

  UTEmu::RawDataMap<double>* m_dataMap;
  UTEmu::RawData<double> m_data;

  std::vector<Noise> m_noise;

  std::vector<HistogramMap> m_noisePerChannelHistograms;
  HistogramMap m_noisePerSensorHistograms;
};
}
