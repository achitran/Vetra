/*
 * UTEmuZeroSuppressionDataMonitorAlgorithm.cpp
 *
 *  Created on: Nov 26, 2014
 *      Author: ADendeka
 */

#include "UTEmuZeroSuppressionDataMonitorAlgorithm.h"
#include "UTEmu/UTEmuDataLocations.h"
#include "UTEmu/UTEmuDataType.h"
#include "GaudiUtils/Aida2ROOT.h"

using namespace UTEmu;

DECLARE_COMPONENT(UTEmu::ZeroSuppressionDataMonitorAlgorithm)

using namespace UTEmu;

ZeroSuppressionDataMonitorAlgorithm::
    ZeroSuppressionDataMonitorAlgorithm(const std::string &name,
                                             ISvcLocator *pSvcLocator)
    : DataMonitorAlgorithm(name, pSvcLocator)
{
  m_input = UTEmu::DataLocations::ZSTES;

}
StatusCode ZeroSuppressionDataMonitorAlgorithm::execute()
{
  DataMonitorAlgorithm::RunPhase l_runPhase =
      DataMonitorAlgorithm::getRunPhase();
  switch (l_runPhase)
  {
  case SKIP:
    return DataMonitorAlgorithm::skippEvent();
  case SAVE_SINGLE_EVENTS:
    return saveSimpleEvents();
  default:
    return fillOnly2DHistogram();
  }
}

StatusCode ZeroSuppressionDataMonitorAlgorithm::getData(std::string input_loc)
{
  m_dataMap = getIfExists<RawZSDataMap>(input_loc);
  if (!m_dataMap)
  {
    error() << "=> there is no input data in " << input_loc << endmsg;
    return StatusCode::FAILURE;
  }
  return StatusCode::SUCCESS;
}

StatusCode ZeroSuppressionDataMonitorAlgorithm::saveSimpleEvents()
{
  setHistoDir(const_cast<char *>("UTEmu__ZeroSuppressionDataMonitor_Events/"));

  if (StatusCode::SUCCESS != getData(m_input))
    return StatusCode::FAILURE;
  if (m_dataMap->isEmpty())
    return StatusCode::SUCCESS;
  for (const auto &[asic_num, Data] : m_dataMap->getData())
  {
    m_data = RawZSData(Data);
    storeEventIntoHistogram(asic_num);
    fillHistogram2D(asic_num);
  }

  m_evtNumber++;
  return StatusCode::SUCCESS;
}

TH2D *ZeroSuppressionDataMonitorAlgorithm::bookHistogram2D(
    const std::string &p_histogramName, const std::string &p_histogramTitle,
    int p_sensorNumber)
{
  int l_ylow = -35;
  int l_yhigh = 35;
  int l_ybin = 70;
  return Gaudi::Utils::Aida2ROOT::aida2root(book2D(
      p_histogramName, p_histogramTitle, -0.5 + RawData<>::getMinChannel(),
      RawData<>::getMaxChannel() - 0.5, p_sensorNumber, l_ylow, l_yhigh,
      l_ybin));
}

std::string ZeroSuppressionDataMonitorAlgorithm::createHistogramTitle(unsigned int asic_num)
{
  return "ZeroSuppresdion - event_" + std::to_string(m_evtNumber) + "_" + std::to_string(asic_num );
}

std::string ZeroSuppressionDataMonitorAlgorithm::createHistogramName(unsigned int asic_num)
{
  return "ZeroSuppredsion_event_" + std::to_string(m_evtNumber) + "_" + std::to_string(asic_num );
}

void ZeroSuppressionDataMonitorAlgorithm::createHistogram2D()
{
  for (unsigned int asic_num = 0; asic_num < m_numOfASICs; asic_num++)
  {
    std::string l_histogramName = "ZeroSuppredsion_vs_channell_" + std::to_string(asic_num );
    std::string l_histogramTtttle = "ZeroSuppredsion vs channell_" + std::to_string(asic_num );
    int l_sensorNum = RawData<>::getnChannelNumber();
    m_histogram2D[asic_num] =
        bookHistogram2D(l_histogramName, l_histogramTtttle, l_sensorNum);
  }
}

StatusCode ZeroSuppressionDataMonitorAlgorithm::fillOnly2DHistogram()
{
  setHistoDir(const_cast<char *>("UTEmu__ZeroSuppressionDataMonitor/"));

  if (StatusCode::SUCCESS != getData(m_input))
    return StatusCode::FAILURE;
  if (m_dataMap->isEmpty())
    return StatusCode::SUCCESS;
  for (const auto &[asic_num, Data] : m_dataMap->getData())
  {
    m_data = RawZSData(Data);
    fillHistogram2D(asic_num);
  }

  DataMonitorAlgorithm::m_evtNumber++;
  return StatusCode::SUCCESS;
}

void ZeroSuppressionDataMonitorAlgorithm::fillHistogram2D(unsigned int asic_num)
{
  int channelNumber = RawData<>::getnChannelNumber();
  for (int chan = 0; chan < channelNumber; chan++)
  {
    auto channelSignal = m_data.getSignal(chan);
    if (0 != channelSignal /* && m_data.getNum(chan) == asic_num*/) // no need to push masked values //check that!
      m_histogram2D[asic_num]->Fill(chan + RawData<>::getMinChannel(), channelSignal);
  }
}

void ZeroSuppressionDataMonitorAlgorithm::fillHistogram(
    TH1D *p_histogram)
{
  int channelNumber = RawData<>::getnChannelNumber();
  for (int chan = 0; chan < channelNumber; chan++)
  {
    p_histogram->SetBinContent(chan, m_data.getSignal(chan));
  }
}

StatusCode ZeroSuppressionDataMonitorAlgorithm::finalize()
{
  DataMonitorAlgorithm::m_outpuProjectionHistogramName = "ProjectionZeroSuppression";
  return DataMonitorAlgorithm::finalize();
}



