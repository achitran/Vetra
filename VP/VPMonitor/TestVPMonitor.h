//
// TestVPMonitor.h
//
// Author: Tamaki Holly McGrath 
// 30.01.20

#ifndef TestVPMonitor_h
#define TestVPMonitor_h

// Includes

#include <string>
#include <vector>

#include "GaudiAlg/GaudiHistoAlg.h"
#include "GaudiAlg/Consumer.h"
#include "GaudiKernel/SystemOfUnits.h"

#include "Event/VPLightCluster.h"

#include <AIDA/IHistogram1D.h>
#include <AIDA/IHistogram2D.h>

using namespace Gaudi::Functional;

// Class structure
class TestVPMonitor : public Consumer<void(const LHCb::VPLightClusters&), Traits::BaseClass_t<GaudiHistoAlg>>
{

public:
    // Constructor - apparently this is standard
    TestVPMonitor(const std::string &name, ISvcLocator *pSvcLocator);
    // Monitor initilaization
    StatusCode initialize() override;
    // Functional operator
    void operator() (const LHCb::VPLightClusters& vpClusters) const override;
/* Might get rid of
protected:
    // Pre-book histograms
    StatusCode prebookHistograms() override;
*/
private:
    // Function to determine sensor and ASIC number
    uint sensor(const LHCb::VPChannelID &vpID) const;
    uint asic(const LHCb::VPChannelID &vpID) const;

    // Histogram objects
    AIDA::IHistogram1D *m_ncl = nullptr; // Number of clusters per event i.e. size of the vpClusters vector
    AIDA::IHistogram1D *m_mnr = nullptr; // Number of clusters per module
    AIDA::IHistogram1D *m_ch = nullptr; // Number of clusters per chip
    AIDA::IHistogram1D *m_se = nullptr; // Number of clusters per sensor
    std::vector<AIDA::IHistogram2D*> m_ao;  // Number of clusters on an ASIC
    std::vector<AIDA::IHistogram2D*> m_sp;  // Cluster occupancy per superpixel on an ASIC 

}; // class TestVPMonitor

#endif //TestVPMonitor_h
