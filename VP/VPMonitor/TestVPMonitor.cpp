//
// TestVPMonitor.cpp
//
// Author: Tamaki Holly McGrath
// 30.01.20

// ---------------------------------------------------------------
// Implementation file for class : TestVPMonitor
// ---------------------------------------------------------------

// Includes
#include "TestVPMonitor.h"

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( TestVPMonitor )

// ===============================================================
// Constructor
// ===============================================================
TestVPMonitor::TestVPMonitor(const std::string &name, ISvcLocator *pSvcLocator) : Consumer(name, pSvcLocator, {KeyValue{"ClusterLocation", LHCb::VPClusterLocation::Light}}) {
    
}

// ===============================================================
// Initialize
// ===============================================================
StatusCode TestVPMonitor::initialize() {

    StatusCode sc = Consumer::initialize();
    if (sc.isFailure()) return sc;

    auto bookHisto = [this](std::string name, std::string title, uint nBins, double min, double max) {
        AIDA::IHistogram1D *h = book1D(name, title, min, max, nBins);
        declareInfo(name, h, title);
        return h;
    };
    auto bookHisto2D = [this](std::string name, std::string title, uint nBinsX, double minX, double maxX, uint nBinsY, double minY, double maxY) {
        AIDA::IHistogram2D *h2 = book2D(name, title, minX, maxX, nBinsX, minY, maxY, nBinsY);
        declareInfo(name, h2, title);
        return h2;
    };

    m_ncl = bookHisto("nVPClustersPerEvent", "VP clusters (Events vs Clusters); Clusters/event; Events", 100, 0., 10000);
    m_mnr = bookHisto("nVPClustersPerModule", "VP clusters per module (Clusters vs Module No.); Module; Clusters", 52, -0.5, 51.5);
    m_ch = bookHisto("nVPClustersPerChip", "VP clusters per chip (Clusters vs Chip No.); Chip; Clusters", 5, -0.5, 4.5);
    m_se = bookHisto("nVPClustersPerSensor", "VP clusters per sensor (Clusters vs Sensor No.); Sensor; Clusters", 6, -0.5, 5.5);

    m_ao.resize(12);
    m_sp.resize(12);
    for (int asicNr = 0; asicNr < 12; asicNr++) {
        // Pixel occupancy per ASIC for all modules
        m_ao[asicNr] = nullptr;
        m_ao[asicNr] = bookHisto2D("VPClustersinASIC" + std::to_string( asicNr + 1 ), "VP cluster position on ASIC " + std::to_string( asicNr + 1 ) + " for all Modules (Row vs Column); Pixel Column; Pixel Row", 32, 0, 256, 32, 0, 256); // Bins of 64 pixels

        // Superpixel occupancy histograms per ASIC for all modules
        m_sp[asicNr] = nullptr;
        m_sp[asicNr] = bookHisto2D("VPClustersPerSuperPixelinASIC" + std::to_string( asicNr + 1 ), "VP cluster position per Super Pixel on ASIC " + std::to_string(asicNr + 1) + " for all Modules (Row vs Column); SP Column; SP Row", 128, 0, 128, 64, 0, 64); 
    }
    

    return sc;
}

// ===============================================================
// Main 
// ===============================================================
void TestVPMonitor::operator() (const LHCb::VPLightClusters& vpClusters) const {
    
    // Fill total number of clusters
    m_ncl->fill(vpClusters.size());

    // Loop over clusters vector
    for (const auto &vpCluster : vpClusters) {
        // Get VP Channel ID
        LHCb::VPChannelID vpID = vpCluster.channelID();

        // Something to get the correct module using initialized detector object, but this might not be necessary - who knows

        // Fill objects
        m_mnr->fill( vpID.module() );
        m_ch->fill( vpID.chip() + 1 );
        m_se->fill( sensor(vpID) );

        // Fill per ASIC for all Modules
	m_ao[asic(vpID)]->fill( vpID.col(), vpID.row() );
        
        // Fill per superpixel per ASIC for all Modules
        m_sp[asic(vpID)]->fill( vpID.col() / 2, vpID.row() / 4 );
        
    }
   
}

// ===============================================================
uint TestVPMonitor::sensor(const LHCb::VPChannelID &vpID) const {
    if ((vpID.sensor() + 1) % 4 == 0) { return 4; }
    else { return (vpID.sensor() + 1) % 4; }
}

uint TestVPMonitor::asic(const LHCb::VPChannelID &vpID) const {
    uint nSensor = sensor(vpID);
    uint nChip = vpID.chip() + 1;
    if (nChip == 1) { return (nSensor * 3) - (nChip + 1) - 1; }
    else if (nChip == 2) { return (nSensor * (nChip + 1)) - 2; }
    else { return nSensor * nChip - 1; }
}










